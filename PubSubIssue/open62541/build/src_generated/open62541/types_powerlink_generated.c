/* Generated from Opc.Ua.POWERLINK.NodeSet2.bsd with script C:/Users/Theresa/Desktop/testnetworksetup/open62541/tools/generate_datatypes.py
 * on host DESKTOP-52E9CUP by user Theresa at 2019-04-04 09:46:32 */

#include "types_powerlink_generated.h"

/* ErrorRegisterBits */
static UA_DataTypeMember ErrorRegisterBits_members[2] = {
{
    UA_TYPENAME("Value") /* .memberName */
    UA_TYPES_BYTESTRING, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("ValidBits") /* .memberName */
    UA_TYPES_BYTESTRING, /* .memberTypeIndex */
    offsetof(UA_ErrorRegisterBits, validBits) - offsetof(UA_ErrorRegisterBits, value) - sizeof(UA_ByteString), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},};

/* PowerlinkAttribute */
static UA_DataTypeMember PowerlinkAttribute_members[2] = {
{
    UA_TYPENAME("Value") /* .memberName */
    UA_TYPES_BYTESTRING, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("ValidBits") /* .memberName */
    UA_TYPES_BYTESTRING, /* .memberTypeIndex */
    offsetof(UA_PowerlinkAttribute, validBits) - offsetof(UA_PowerlinkAttribute, value) - sizeof(UA_ByteString), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},};

/* PowerlinkErrorEntryDataType */
static UA_DataTypeMember PowerlinkErrorEntryDataType_members[4] = {
{
    UA_TYPENAME("EntryType") /* .memberName */
    UA_TYPES_UINT16, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("ErrorCode") /* .memberName */
    UA_TYPES_UINT16, /* .memberTypeIndex */
    offsetof(UA_PowerlinkErrorEntryDataType, errorCode) - offsetof(UA_PowerlinkErrorEntryDataType, entryType) - sizeof(UA_UInt16), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("TimeStamp") /* .memberName */
    UA_TYPES_UINT64, /* .memberTypeIndex */
    offsetof(UA_PowerlinkErrorEntryDataType, timeStamp) - offsetof(UA_PowerlinkErrorEntryDataType, errorCode) - sizeof(UA_UInt16), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("AdditionalInformation") /* .memberName */
    UA_TYPES_UINT64, /* .memberTypeIndex */
    offsetof(UA_PowerlinkErrorEntryDataType, additionalInformation) - offsetof(UA_PowerlinkErrorEntryDataType, timeStamp) - sizeof(UA_UInt64), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},};

/* PowerlinkIpAddressDataType */
static UA_DataTypeMember PowerlinkIpAddressDataType_members[4] = {
{
    UA_TYPENAME("B1") /* .memberName */
    UA_TYPES_BYTE, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("B2") /* .memberName */
    UA_TYPES_BYTE, /* .memberTypeIndex */
    offsetof(UA_PowerlinkIpAddressDataType, b2) - offsetof(UA_PowerlinkIpAddressDataType, b1) - sizeof(UA_Byte), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("B3") /* .memberName */
    UA_TYPES_BYTE, /* .memberTypeIndex */
    offsetof(UA_PowerlinkIpAddressDataType, b3) - offsetof(UA_PowerlinkIpAddressDataType, b2) - sizeof(UA_Byte), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("B4") /* .memberName */
    UA_TYPES_BYTE, /* .memberTypeIndex */
    offsetof(UA_PowerlinkIpAddressDataType, b4) - offsetof(UA_PowerlinkIpAddressDataType, b3) - sizeof(UA_Byte), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},};

/* PowerlinkPDOMappingEntryDataType */
static UA_DataTypeMember PowerlinkPDOMappingEntryDataType_members[5] = {
{
    UA_TYPENAME("Length") /* .memberName */
    UA_TYPES_UINT16, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Offset") /* .memberName */
    UA_TYPES_UINT16, /* .memberTypeIndex */
    offsetof(UA_PowerlinkPDOMappingEntryDataType, offset) - offsetof(UA_PowerlinkPDOMappingEntryDataType, length) - sizeof(UA_UInt16), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Reserved") /* .memberName */
    UA_TYPES_BYTE, /* .memberTypeIndex */
    offsetof(UA_PowerlinkPDOMappingEntryDataType, reserved) - offsetof(UA_PowerlinkPDOMappingEntryDataType, offset) - sizeof(UA_UInt16), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("SubIndex") /* .memberName */
    UA_TYPES_BYTE, /* .memberTypeIndex */
    offsetof(UA_PowerlinkPDOMappingEntryDataType, subIndex) - offsetof(UA_PowerlinkPDOMappingEntryDataType, reserved) - sizeof(UA_Byte), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Index") /* .memberName */
    UA_TYPES_UINT16, /* .memberTypeIndex */
    offsetof(UA_PowerlinkPDOMappingEntryDataType, index) - offsetof(UA_PowerlinkPDOMappingEntryDataType, subIndex) - sizeof(UA_Byte), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},};

/* PowerlinkNMTResetCmdEnumeration */
#define PowerlinkNMTResetCmdEnumeration_members NULL

/* PowerlinkNMTStateEnumeration */
#define PowerlinkNMTStateEnumeration_members NULL
const UA_DataType UA_TYPES_POWERLINK[UA_TYPES_POWERLINK_COUNT] = {
/* ErrorRegisterBits */
{
    UA_TYPENAME("ErrorRegisterBits") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {26}}, /* .typeId */
    sizeof(UA_ErrorRegisterBits), /* .memSize */
    UA_TYPES_POWERLINK_ERRORREGISTERBITS, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    2, /* .membersSize */
    36, /* .binaryEncodingId */
    ErrorRegisterBits_members /* .members */
},
/* PowerlinkAttribute */
{
    UA_TYPENAME("PowerlinkAttribute") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {25}}, /* .typeId */
    sizeof(UA_PowerlinkAttribute), /* .memSize */
    UA_TYPES_POWERLINK_POWERLINKATTRIBUTE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    2, /* .membersSize */
    33, /* .binaryEncodingId */
    PowerlinkAttribute_members /* .members */
},
/* PowerlinkErrorEntryDataType */
{
    UA_TYPENAME("PowerlinkErrorEntryDataType") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {27}}, /* .typeId */
    sizeof(UA_PowerlinkErrorEntryDataType), /* .memSize */
    UA_TYPES_POWERLINK_POWERLINKERRORENTRYDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    true
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && offsetof(UA_PowerlinkErrorEntryDataType, errorCode) == (offsetof(UA_PowerlinkErrorEntryDataType, entryType) + sizeof(UA_UInt16))
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && offsetof(UA_PowerlinkErrorEntryDataType, timeStamp) == (offsetof(UA_PowerlinkErrorEntryDataType, errorCode) + sizeof(UA_UInt16))
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && offsetof(UA_PowerlinkErrorEntryDataType, additionalInformation) == (offsetof(UA_PowerlinkErrorEntryDataType, timeStamp) + sizeof(UA_UInt64)), /* .overlayable */
    4, /* .membersSize */
    53, /* .binaryEncodingId */
    PowerlinkErrorEntryDataType_members /* .members */
},
/* PowerlinkIpAddressDataType */
{
    UA_TYPENAME("PowerlinkIpAddressDataType") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {29}}, /* .typeId */
    sizeof(UA_PowerlinkIpAddressDataType), /* .memSize */
    UA_TYPES_POWERLINK_POWERLINKIPADDRESSDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    true
		 && true
		 && true
		 && offsetof(UA_PowerlinkIpAddressDataType, b2) == (offsetof(UA_PowerlinkIpAddressDataType, b1) + sizeof(UA_Byte))
		 && true
		 && offsetof(UA_PowerlinkIpAddressDataType, b3) == (offsetof(UA_PowerlinkIpAddressDataType, b2) + sizeof(UA_Byte))
		 && true
		 && offsetof(UA_PowerlinkIpAddressDataType, b4) == (offsetof(UA_PowerlinkIpAddressDataType, b3) + sizeof(UA_Byte)), /* .overlayable */
    4, /* .membersSize */
    32, /* .binaryEncodingId */
    PowerlinkIpAddressDataType_members /* .members */
},
/* PowerlinkPDOMappingEntryDataType */
{
    UA_TYPENAME("PowerlinkPDOMappingEntryDataType") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {30}}, /* .typeId */
    sizeof(UA_PowerlinkPDOMappingEntryDataType), /* .memSize */
    UA_TYPES_POWERLINK_POWERLINKPDOMAPPINGENTRYDATATYPE, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    true
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && offsetof(UA_PowerlinkPDOMappingEntryDataType, offset) == (offsetof(UA_PowerlinkPDOMappingEntryDataType, length) + sizeof(UA_UInt16))
		 && true
		 && offsetof(UA_PowerlinkPDOMappingEntryDataType, reserved) == (offsetof(UA_PowerlinkPDOMappingEntryDataType, offset) + sizeof(UA_UInt16))
		 && true
		 && offsetof(UA_PowerlinkPDOMappingEntryDataType, subIndex) == (offsetof(UA_PowerlinkPDOMappingEntryDataType, reserved) + sizeof(UA_Byte))
		 && UA_BINARY_OVERLAYABLE_INTEGER
		 && offsetof(UA_PowerlinkPDOMappingEntryDataType, index) == (offsetof(UA_PowerlinkPDOMappingEntryDataType, subIndex) + sizeof(UA_Byte)), /* .overlayable */
    5, /* .membersSize */
    40, /* .binaryEncodingId */
    PowerlinkPDOMappingEntryDataType_members /* .members */
},
/* PowerlinkNMTResetCmdEnumeration */
{
    UA_TYPENAME("PowerlinkNMTResetCmdEnumeration") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {28}}, /* .typeId */
    sizeof(UA_PowerlinkNMTResetCmdEnumeration), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    0, /* .binaryEncodingId */
    PowerlinkNMTResetCmdEnumeration_members /* .members */
},
/* PowerlinkNMTStateEnumeration */
{
    UA_TYPENAME("PowerlinkNMTStateEnumeration") /* .typeName */
    {3, UA_NODEIDTYPE_NUMERIC, {24}}, /* .typeId */
    sizeof(UA_PowerlinkNMTStateEnumeration), /* .memSize */
    UA_TYPES_INT32, /* .typeIndex */
    UA_DATATYPEKIND_ENUM, /* .typeKind */
    true, /* .pointerFree */
    UA_BINARY_OVERLAYABLE_INTEGER, /* .overlayable */
    0, /* .membersSize */
    0, /* .binaryEncodingId */
    PowerlinkNMTStateEnumeration_members /* .members */
},
};

