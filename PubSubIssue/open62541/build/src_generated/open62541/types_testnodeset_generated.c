/* Generated from testtypes.bsd with script C:/Users/Theresa/Desktop/testnetworksetup/open62541/tools/generate_datatypes.py
 * on host DESKTOP-52E9CUP by user Theresa at 2019-04-04 09:46:32 */

#include "types_testnodeset_generated.h"

/* Point */
static UA_DataTypeMember Point_members[2] = {
{
    UA_TYPENAME("X") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Y") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_Point, y) - offsetof(UA_Point, x) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},};

/* NestedPoint */
static UA_DataTypeMember NestedPoint_members[3] = {
{
    UA_TYPENAME("X") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Y") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_NestedPoint, y) - offsetof(UA_NestedPoint, x) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Point1") /* .memberName */
    UA_TYPES_TESTNODESET_POINT, /* .memberTypeIndex */
    offsetof(UA_NestedPoint, point1) - offsetof(UA_NestedPoint, y) - sizeof(UA_Double), /* .padding */
    false, /* .namespaceZero */
    false /* .isArray */
},};

/* PointWithArray */
static UA_DataTypeMember PointWithArray_members[4] = {
{
    UA_TYPENAME("X") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Y") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_PointWithArray, y) - offsetof(UA_PointWithArray, x) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Z") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_PointWithArray, z) - offsetof(UA_PointWithArray, y) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Array1") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_PointWithArray, array1Size) - offsetof(UA_PointWithArray, z) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    true /* .isArray */
},};

/* PointWithPointArray */
static UA_DataTypeMember PointWithPointArray_members[4] = {
{
    UA_TYPENAME("X") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    0, /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Y") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_PointWithPointArray, y) - offsetof(UA_PointWithPointArray, x) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Z") /* .memberName */
    UA_TYPES_DOUBLE, /* .memberTypeIndex */
    offsetof(UA_PointWithPointArray, z) - offsetof(UA_PointWithPointArray, y) - sizeof(UA_Double), /* .padding */
    true, /* .namespaceZero */
    false /* .isArray */
},
{
    UA_TYPENAME("Array1") /* .memberName */
    UA_TYPES_TESTNODESET_POINT, /* .memberTypeIndex */
    offsetof(UA_PointWithPointArray, array1Size) - offsetof(UA_PointWithPointArray, z) - sizeof(UA_Double), /* .padding */
    false, /* .namespaceZero */
    true /* .isArray */
},};
const UA_DataType UA_TYPES_TESTNODESET[UA_TYPES_TESTNODESET_COUNT] = {
/* Point */
{
    UA_TYPENAME("Point") /* .typeName */
    {2, UA_NODEIDTYPE_NUMERIC, {10001}}, /* .typeId */
    sizeof(UA_Point), /* .memSize */
    UA_TYPES_TESTNODESET_POINT, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    true
		 && UA_BINARY_OVERLAYABLE_FLOAT
		 && UA_BINARY_OVERLAYABLE_FLOAT
		 && offsetof(UA_Point, y) == (offsetof(UA_Point, x) + sizeof(UA_Double)), /* .overlayable */
    2, /* .membersSize */
    5002, /* .binaryEncodingId */
    Point_members /* .members */
},
/* NestedPoint */
{
    UA_TYPENAME("NestedPoint") /* .typeName */
    {2, UA_NODEIDTYPE_NUMERIC, {10008}}, /* .typeId */
    sizeof(UA_NestedPoint), /* .memSize */
    UA_TYPES_TESTNODESET_NESTEDPOINT, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    true, /* .pointerFree */
    true
		 && UA_BINARY_OVERLAYABLE_FLOAT
		 && UA_BINARY_OVERLAYABLE_FLOAT
		 && offsetof(UA_NestedPoint, y) == (offsetof(UA_NestedPoint, x) + sizeof(UA_Double))
		 && true
		 && UA_BINARY_OVERLAYABLE_FLOAT
		 && UA_BINARY_OVERLAYABLE_FLOAT
		 && offsetof(UA_Point, y) == (offsetof(UA_Point, x) + sizeof(UA_Double))
		 && offsetof(UA_NestedPoint, point1) == (offsetof(UA_NestedPoint, y) + sizeof(UA_Double)), /* .overlayable */
    3, /* .membersSize */
    5003, /* .binaryEncodingId */
    NestedPoint_members /* .members */
},
/* PointWithArray */
{
    UA_TYPENAME("PointWithArray") /* .typeName */
    {2, UA_NODEIDTYPE_NUMERIC, {3003}}, /* .typeId */
    sizeof(UA_PointWithArray), /* .memSize */
    UA_TYPES_TESTNODESET_POINTWITHARRAY, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    4, /* .membersSize */
    5004, /* .binaryEncodingId */
    PointWithArray_members /* .members */
},
/* PointWithPointArray */
{
    UA_TYPENAME("PointWithPointArray") /* .typeName */
    {2, UA_NODEIDTYPE_NUMERIC, {3004}}, /* .typeId */
    sizeof(UA_PointWithPointArray), /* .memSize */
    UA_TYPES_TESTNODESET_POINTWITHPOINTARRAY, /* .typeIndex */
    UA_DATATYPEKIND_STRUCTURE, /* .typeKind */
    false, /* .pointerFree */
    false, /* .overlayable */
    4, /* .membersSize */
    0, /* .binaryEncodingId */
    PointWithPointArray_members /* .members */
},
};

