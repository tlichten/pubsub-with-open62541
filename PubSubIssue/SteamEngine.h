#ifndef STEAMENGINE_H_
#define STEAMENGINE_H_

#include "open62541.h"

#ifdef __cplusplus
extern "C" {
#endif

void turnOnOffHeating(UA_Server* server);

/* defines all sensor types */
void defineBaseSensorType(UA_Server* server);

/* add all instances of each sensor */
void addSensorInstance(UA_Server* server, char* name, int nsIndex, int numIdent);

/* CTORS */
UA_StatusCode baseSensorTypeConstructor(UA_Server* server, 
                    const UA_NodeId *sessionId, void *sessionContext,
                    const UA_NodeId *typeId, void *typeContext,
                    const UA_NodeId *nodeId, void **nodeContext);

/* add all CTORS to the server */
void addSensorTypeConstructor(UA_Server* server);

/* Methods for connecting the serverside sensor variables with the physical sensor values. This is done with the Value Callback Method */
void addValueCallback(UA_Server *server, int nsIndex, int numIdent);


#ifdef __cplusplus
} // extern "C"
#endif

#endif /* STEAMENGINE_H_ */



