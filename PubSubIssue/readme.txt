----------------------------------------------------------------------------------
-  PUBLISH values using open62541  -----------------------------------------------
----------------------------------------------------------------------------------
to build the publish application, use:
gcc -o ServerPublisher open62541.c SEServerPublisher.c SteamEngine.c

The publish application publishes the current Server's Datetime variable and 
a default temperature value. Publishing the Server's Datetime variable is working
as expected (because it is the copied from the tutorial_pubsub_publish.c sample
from the open62541 documentation). But publishing the temperature value (double)
is not working yet.

----------------------------------------------------------------------------------
-  SUBSCRIBE to published values using open62541  --------------------------------
----------------------------------------------------------------------------------
to build the subscribe application, you will have to build it with
the open62541 visual studio project within the tutorial_pubsub_subscribe.c tutorial.
Just open the open62541 visual studio project and build the tutorial_pubsub_subscribe project.

For the subscribe application, I am using the tutorial_pubsub_subscribe.c sample 
from the open62541 documentation. This application subscribes to the published 
datetime variable and prints it out to the console, which is working fine.
ALSO, I have added some lines of code in order to parse the temperature value as well.
I have done it in the same manner as with the datetime. But it is still not working!