#include <signal.h>
#include "open62541.h"
#include "SteamEngine.h"
#include "MCP3008Driver.h"
#include "DHT11.h"
#include "rpm.h"
#include <stdio.h>

/* predefined identifier for later use */
UA_NodeId baseSensorTypeId = {1, UA_NODEIDTYPE_NUMERIC, {1001}};

void defineBaseSensorType(UA_Server* server){

    /* Define the object type for "currentValeratureSensor" */
    UA_ObjectTypeAttributes tstAttr = UA_ObjectTypeAttributes_default;
    tstAttr.displayName = UA_LOCALIZEDTEXT("en-US", "BaseSensorType");
    UA_Server_addObjectTypeNode(server, UA_NODEID_NULL,
                                UA_NODEID_NUMERIC(0, UA_NS0ID_BASEOBJECTTYPE),
                                UA_NODEID_NUMERIC(0, UA_NS0ID_HASSUBTYPE),
                                UA_QUALIFIEDNAME(1, "BaseSensorType"), tstAttr,
                                NULL, &baseSensorTypeId);

    /* Define the attributes held by the variables */
    UA_String str = UA_STRING("default");
    UA_VariableAttributes sensorNameAttr = UA_VariableAttributes_default;
    sensorNameAttr.displayName = UA_LOCALIZEDTEXT("en-US", "SensorName");
    sensorNameAttr.accessLevel = UA_ACCESSLEVELMASK_READ;
    sensorNameAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    UA_Variant_setScalar(&sensorNameAttr.value, &str, &UA_TYPES[UA_TYPES_STRING]);

    UA_NodeId sensorNameId;
    UA_Server_addVariableNode(server, UA_NODEID_NULL, baseSensorTypeId,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                              UA_QUALIFIEDNAME(1, "SensorName"),
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), sensorNameAttr, NULL, &sensorNameId);
    
    /* Make the sensor name mandatory */
    UA_Server_addReference(server, sensorNameId,
                           UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE),
                           UA_EXPANDEDNODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULE_MANDATORY), true);

	/* Location of the sensor */
    str = UA_STRING("default");
    UA_VariableAttributes locationAttr = UA_VariableAttributes_default;
    locationAttr.displayName = UA_LOCALIZEDTEXT("en-US", "Location");
    locationAttr.accessLevel = UA_ACCESSLEVELMASK_READ;
    locationAttr.dataType = UA_TYPES[UA_TYPES_STRING].typeId;
    UA_Variant_setScalar(&locationAttr.value, &str, &UA_TYPES[UA_TYPES_STRING]);

    UA_NodeId sensorLocationId;
	// BaseSensorTypeId is the parent nodeid here!
    UA_Server_addVariableNode(server, UA_NODEID_NULL, baseSensorTypeId,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                              UA_QUALIFIEDNAME(1, "Location"),
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), locationAttr, NULL, &sensorLocationId);

    /* Make the sensor location mandatory */
    UA_Server_addReference(server, sensorLocationId,
                           UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE),
                           UA_EXPANDEDNODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULE_MANDATORY), true);

	/* Current sensor value of the sensor */
    UA_Double val = 47.11;    
    UA_VariableAttributes sensorValAttr = UA_VariableAttributes_default;
    UA_Variant_setScalar(&sensorValAttr.value, &val, &UA_TYPES[UA_TYPES_DOUBLE]);
    sensorValAttr.description = UA_LOCALIZEDTEXT("en-US", "SensorValue");
    sensorValAttr.displayName = UA_LOCALIZEDTEXT("en-US", "SensorValue");
    sensorValAttr.dataType = UA_TYPES[UA_TYPES_DOUBLE].typeId;
    sensorValAttr.accessLevel = UA_ACCESSLEVELMASK_READ | UA_ACCESSLEVELMASK_WRITE;

    /* Add the variable to the information model */
    UA_NodeId sensorValId = UA_NODEID_STRING(1, "sensor.value");
	// BaseSensorTypeId is the parent nodeid here!
    UA_Server_addVariableNode(server, sensorValId, baseSensorTypeId,
                              UA_NODEID_NUMERIC(0, UA_NS0ID_HASCOMPONENT),
                              UA_QUALIFIEDNAME(1, "sensor.value"),
                              UA_NODEID_NUMERIC(0, UA_NS0ID_BASEDATAVARIABLETYPE), sensorValAttr, NULL, &sensorValId);


    /* Make the sensor location mandatory */
    UA_Server_addReference(server, sensorValId,
                           UA_NODEID_NUMERIC(0, UA_NS0ID_HASMODELLINGRULE),
                           UA_EXPANDEDNODEID_NUMERIC(0, UA_NS0ID_MODELLINGRULE_MANDATORY), true);

}


/* add all instances of each sensor */
void addSensorInstance(UA_Server* server, char* name, int nsIndex, int numIdent){
	UA_NodeId myNodeId = UA_NODEID_NUMERIC(nsIndex, numIdent);
    UA_ObjectAttributes oAttr = UA_ObjectAttributes_default;
    oAttr.displayName = UA_LOCALIZEDTEXT("en-US", name);
    UA_Server_addObjectNode(server, myNodeId,
                            UA_NODEID_NUMERIC(0, UA_NS0ID_OBJECTSFOLDER),
                            UA_NODEID_NUMERIC(0, UA_NS0ID_ORGANIZES),
                            UA_QUALIFIEDNAME(1, name),
                            baseSensorTypeId, /* this refers to the object type
                                           identifier */
                            oAttr, NULL, NULL);
}

/* CTORS */
UA_StatusCode baseSensorTypeConstructor(UA_Server* server, 
                    const UA_NodeId *sessionId, void *sessionContext,
                    const UA_NodeId *typeId, void *typeContext,
                    const UA_NodeId *nodeId, void **nodeContext){

    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND, "New sensor created");


    /* At this point we could replace the node context .. */

    return UA_STATUSCODE_GOOD;
}



/* add all CTORS to the server */
void addSensorTypeConstructor(UA_Server* server){

    UA_NodeTypeLifecycle lifecycle;
    lifecycle.constructor = baseSensorTypeConstructor;
    lifecycle.destructor = NULL;
    UA_Server_setNodeTypeLifecycle(server, baseSensorTypeId, lifecycle);
}


/* add variable value callback for changing the value of the server variable binded to the physical sensor value */
void beforeRead(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeid, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data) {
	UA_Variant value;
	UA_Double currentVal = 0.0;
	DHT11_data dht;
	switch (nodeid->identifier.numeric){
		case 50220: 
			// boiler
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 11.11; // default value if executing without rasperry pi
			break;
		case 50224: 
			// heater
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 22.22; // default value if executing without rasperry pi
			break;
		case 50228: 
			// environment
			read_dht_data(&dht);
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 33.33; // default value if executing without rasperry pi
			break;
		case 50232: 
			// pressure
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 44.44; // default value if executing without rasperry pi
			break;
		case 50236: 
			// rpm
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 55.55; // default value if executing without rasperry pi
			break;
		case 50240: 
			// humidity
			read_dht_data(&dht);
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 66.66; // default value if executing without rasperry pi
			break;
		case 50244: 
			// dynamo
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 77.77; // default value if executing without rasperry pi
			break;
		case 50248: 
			// power supply
			UA_Variant_setScalar(&value, &currentVal, &UA_TYPES[UA_TYPES_DOUBLE]);
			currentVal = 88.88; // default value if executing without rasperry pi
			break;
		default: 
			printf("identifier not found!"); 
			break;
	}
    UA_Server_writeValue(server, *nodeid, value);
}

void afterWrite(UA_Server *server,
               const UA_NodeId *sessionId, void *sessionContext,
               const UA_NodeId *nodeId, void *nodeContext,
               const UA_NumericRange *range, const UA_DataValue *data) {
    UA_LOG_INFO(UA_Log_Stdout, UA_LOGCATEGORY_USERLAND,
                "The variable was updated");
}

// add a value callback mechanism to a specified variable node 
void addValueCallback(UA_Server *server, int nsIndex, int numIdent) {
    UA_NodeId currentNodeId = UA_NODEID_NUMERIC(nsIndex, numIdent);
    UA_ValueCallback callback ;
    callback.onRead = beforeRead;
    callback.onWrite = afterWrite;
    UA_Server_setVariableNode_valueCallback(server, currentNodeId, callback);
} 



